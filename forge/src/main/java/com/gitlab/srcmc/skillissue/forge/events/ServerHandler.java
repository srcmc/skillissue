/*
 * This file is part of Skill Issue.
 * Copyright (c) 2024, HDainester, All rights reserved.
 *
 * Skill Issue is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Skill Issue is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along
 * with Skill Issue. If not, see <http://www.gnu.org/licenses/lgpl>.
 */
package com.gitlab.srcmc.skillissue.forge.events;

import com.gitlab.srcmc.skillissue.ModCommon;
import com.gitlab.srcmc.skillissue.forge.ModForge;
import com.gitlab.srcmc.skillissue.forge.commands.ChunkXpCommands;
import com.gitlab.srcmc.skillissue.forge.commands.PlayerLevelCommands;

import net.minecraftforge.event.RegisterCommandsEvent;
import net.minecraftforge.event.TickEvent.ServerTickEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;

@Mod.EventBusSubscriber(modid = ModCommon.MOD_ID, bus = Bus.FORGE)
public class ServerHandler {
    @SubscribeEvent
    public static void onServerTick(ServerTickEvent event) {
        ModForge.CHUNK_XP_POOLS.regenXpPools();
    }

	@SubscribeEvent
	public static void onCommandRegistry(final RegisterCommandsEvent event) {
		PlayerLevelCommands.register(event.getDispatcher());
		ChunkXpCommands.register(event.getDispatcher());
    }
}
