/*
 * This file is part of Skill Issue.
 * Copyright (c) 2024, HDainester, All rights reserved.
 *
 * Skill Issue is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Skill Issue is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along
 * with Skill Issue. If not, see <http://www.gnu.org/licenses/lgpl>.
 */
package com.gitlab.srcmc.skillissue.forge;

import com.gitlab.srcmc.skillissue.ModCommon;

import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.ItemStack;

public class ModCreativeTab extends CreativeModeTab {
    private static ModCreativeTab instance;

    public ModCreativeTab() {
        super(CreativeModeTab.TABS.length, ModCommon.MOD_ID);
    }

    @Override
    public ItemStack makeIcon() {
        return ModRegistries.Blocks.XP_SENSOR.get().asItem().getDefaultInstance();
    }

    public static ModCreativeTab get() {
        return instance == null ? (instance = new ModCreativeTab()) : instance;
    }
}
