/*
 * This file is part of Skill Issue.
 * Copyright (c) 2024, HDainester, All rights reserved.
 *
 * Skill Issue is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Skill Issue is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along
 * with Skill Issue. If not, see <http://www.gnu.org/licenses/lgpl>.
 */
package com.gitlab.srcmc.skillissue.forge.api.attributes;

import com.gitlab.srcmc.skillissue.forge.ModForge;

import net.minecraft.world.entity.ai.attributes.Attribute;
import net.minecraft.world.entity.player.Player;

public class AttributeModification {
    public final double bonus, minFactor, maxFactor;
    public final Attribute attribute;

    public AttributeModification(Attribute attribute, double bonus, double minFactor, double maxFactor) {
        this.bonus = bonus;
        this.minFactor = minFactor;
        this.maxFactor = maxFactor;
        this.attribute = attribute;
    }

    public void apply(Player player, int level) {
        var factor = minFactor + (maxFactor - minFactor)*Math.min(1, level/(double)ModForge.COMMON_CONFIG.getMaxPlayerLevel());
        var modAccess = ModForge.PLAYER_MODIFIERS.get(player, attribute);
        modAccess.setBonus(bonus);
        modAccess.setFactor(factor);
    }
}
