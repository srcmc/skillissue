/*
 * This file is part of Skill Issue.
 * Copyright (c) 2024, HDainester, All rights reserved.
 *
 * Skill Issue is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Skill Issue is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along
 * with Skill Issue. If not, see <http://www.gnu.org/licenses/lgpl>.
 */
package com.gitlab.srcmc.skillissue.forge;

import com.gitlab.srcmc.skillissue.ModCommon;
import com.gitlab.srcmc.skillissue.forge.api.attributes.PlayerModifierMap;
import com.gitlab.srcmc.skillissue.forge.api.world.ChunkXpPoolMap;
import com.gitlab.srcmc.skillissue.forge.config.CommonConfig;

import net.minecraftforge.fml.common.Mod;

@Mod(ModCommon.MOD_ID)
public class ModForge {
    public static final ChunkXpPoolMap CHUNK_XP_POOLS = new ChunkXpPoolMap();
    public static final PlayerModifierMap PLAYER_MODIFIERS = new PlayerModifierMap();
    public static final CommonConfig COMMON_CONFIG = new CommonConfig();

    public ModForge() {
        COMMON_CONFIG.register(this);
    }
}
