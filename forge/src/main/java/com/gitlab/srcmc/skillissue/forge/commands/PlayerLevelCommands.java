/*
 * This file is part of Skill Issue.
 * Copyright (c) 2024, HDainester, All rights reserved.
 *
 * Skill Issue is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Skill Issue is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along
 * with Skill Issue. If not, see <http://www.gnu.org/licenses/lgpl>.
 */
package com.gitlab.srcmc.skillissue.forge.commands;

import com.gitlab.srcmc.skillissue.ModCommon;
import com.gitlab.srcmc.skillissue.forge.api.milestones.Milestone;
import com.gitlab.srcmc.skillissue.forge.api.milestones.Milestones;
import com.gitlab.srcmc.skillissue.forge.capabilities.IPlayerLevelCapability;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.IntegerArgumentType;

import net.minecraft.commands.CommandSourceStack;
import net.minecraft.commands.Commands;
import net.minecraft.commands.arguments.EntityArgument;
import net.minecraft.network.chat.Component;

public final class PlayerLevelCommands {
    private PlayerLevelCommands() {}

    public static void register(CommandDispatcher<CommandSourceStack> dispatcher) {
        dispatcher.register(Commands.literal(ModCommon.MOD_ID)
            .requires(css -> css.hasPermission(1))
            .then(Commands.literal("level")
                .then(Commands.literal("get")
                    .then(Commands.argument("target", EntityArgument.player())
                        .executes(context -> {
                            var player = EntityArgument.getPlayer(context, "target");
                            var level = IPlayerLevelCapability.getMaxPlayerLevel(player);
                            context.getSource().sendSuccess(Component.literal(String.valueOf(level)), false);
                            return level;
                        })))
                .then(Commands.literal("set")
                    .requires(css -> css.hasPermission(2))
                    .then(Commands.argument("targets", EntityArgument.players())
                        .then(Commands.argument("value", IntegerArgumentType.integer(0))
                            .executes(context -> {
                                    var targets = EntityArgument.getPlayers(context, "targets");
                                    var level = IntegerArgumentType.getInteger(context, "value");

                                    for(var player : targets) {
                                        IPlayerLevelCapability.setMaxPlayerLevel(player, level);
                                        Milestones.updateAttributes(player);
                                        Milestones.updateRewards(player, 0, Milestone.CommandType.LEVEL_UP);
                                    }

                                    return level;
                                }))))));
    }
}
