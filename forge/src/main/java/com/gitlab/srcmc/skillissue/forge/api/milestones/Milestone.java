/*
 * This file is part of Skill Issue.
 * Copyright (c) 2024, HDainester, All rights reserved.
 *
 * Skill Issue is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Skill Issue is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along
 * with Skill Issue. If not, see <http://www.gnu.org/licenses/lgpl>.
 */
package com.gitlab.srcmc.skillissue.forge.api.milestones;

import java.util.List;

import com.mojang.brigadier.ParseResults;

import net.minecraft.commands.CommandSourceStack;

public class Milestone implements Comparable<Milestone> {
    public static class CommandType {
        public static final char ANY = (char)0xFFFFF;
        public static final char LEVEL_UP = 1;
        public static final char RESPAWN = 2;
        public static final char LOGIN = 4;
    }

    public final List<ParseResults<CommandSourceStack>> commands;
    public final char commandsType;
    public final boolean isVolatile;
    public final int level;

    public Milestone(int level, List<ParseResults<CommandSourceStack>> commands, char commandsType, boolean isVolatile) {
        this.commandsType = commandsType;
        this.commands = commands;
        this.isVolatile = isVolatile;
        this.level = level;
    }

    @Override
    public int compareTo(Milestone other) {
        return Integer.compare(level, other.level);
    }

    public static Milestone dummy(int level) {
        return new Milestone(level, null, '\0', false);
    }
}
