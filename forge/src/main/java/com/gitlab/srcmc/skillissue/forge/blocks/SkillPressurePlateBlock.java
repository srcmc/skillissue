/*
 * This file is part of Skill Issue.
 * Copyright (c) 2024, HDainester, All rights reserved.
 *
 * Skill Issue is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Skill Issue is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along
 * with Skill Issue. If not, see <http://www.gnu.org/licenses/lgpl>.
 */
package com.gitlab.srcmc.skillissue.forge.blocks;

import com.gitlab.srcmc.skillissue.forge.ModCapabilities;
import com.gitlab.srcmc.skillissue.forge.ModForge;

import net.minecraft.core.BlockPos;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.PressurePlateBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.IntegerProperty;
import net.minecraft.world.phys.AABB;

public class SkillPressurePlateBlock extends PressurePlateBlock {
    public static final IntegerProperty POWER = BlockStateProperties.POWER;

    public SkillPressurePlateBlock() {
        super(Sensitivity.MOBS, Properties.copy(Blocks.STONE_PRESSURE_PLATE));
    }

    @Override
    protected int getSignalForState(BlockState state) {
        return state.getValue(POWER);
    }

    @Override
    protected BlockState setSignalForState(BlockState state, int power) {
        state = super.setSignalForState(state, power);
        return state.setValue(POWER, Integer.valueOf(power));
    }

    @Override
    protected int getSignalStrength(Level level, BlockPos pos) {
        AABB aabb = TOUCH_AABB.move(pos);
        var list = level.getEntitiesOfClass(Player.class, aabb);
        var maxLevel = 0;

        for(var player : list) {
            if(!player.isIgnoringBlockTriggers()) {
                var levelCap = player.getCapability(ModCapabilities.LEVEL).orElse(null);

                if(levelCap != null) {
                    maxLevel = Math.max(maxLevel, levelCap.getMaxLevel());
                }
            }
        }

        return list.size() > 0 ? (int)(1 + 14*(maxLevel/(double)ModForge.COMMON_CONFIG.getMaxPlayerLevel())) : 0;
    }

    @Override
    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder) {
        super.createBlockStateDefinition(builder);
        builder.add(POWER);
    }
}
