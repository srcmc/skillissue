/*
 * This file is part of Skill Issue.
 * Copyright (c) 2024, HDainester, All rights reserved.
 *
 * Skill Issue is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Skill Issue is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along
 * with Skill Issue. If not, see <http://www.gnu.org/licenses/lgpl>.
 */
package com.gitlab.srcmc.skillissue.forge.api.world;

import java.util.HashMap;
import java.util.Map;
import com.gitlab.srcmc.skillissue.forge.ModForge;

import net.minecraft.world.level.ChunkPos;

public class ChunkXpPoolMap {
    private final Map<ChunkPos, XpPool> chunkXpPools = new HashMap<>();

    private class XpPool {
        public int value;

        public XpPool(int value) {
            this.value = value;
        }
    }

    /**
     * Decrements the scaled xp of all chunk xp pools by chunk_xp_regen_per_tick. If an
     * xp pool for a chunk reaches 0 or less it will not be tracked anymore.
     */
    public void regenXpPools() {
        var it = chunkXpPools.keySet().iterator();

        while(it.hasNext()) {
            var chunkPos = it.next();
            var xpPool = chunkXpPools.get(chunkPos);
            xpPool.value -= ModForge.COMMON_CONFIG.getChunkXpRegenPerTick();

            if(xpPool.value <= 0) {
                it.remove();
            }
        }
    }

    /**
     * Adds the given amount of xp to the xp pools of the chunk and its direct 8
     * neighbours (scaled by chunk_xp_scale).
     * 
     * @param chunkPos Position of the chunk to add xp to.
     * @param xpValue Amount of xp to add.
     */
    public void addXpToPools(ChunkPos chunkPos, int xpValue) {
        xpValue *= ModForge.COMMON_CONFIG.getChunkXpScale();
        
        // active chunks (3x3)
        for(int x, z = -1; z < 2; z ++) {
            for(x = -1; x < 2; x++) {
                var offsetChunkPos = new ChunkPos(chunkPos.x + x, chunkPos.z + z);
                var xpPool = chunkXpPools.get(offsetChunkPos);

                if(xpPool == null) {
                    chunkXpPools.put(offsetChunkPos, new XpPool(xpValue));
                } else {
                    xpPool.value += xpValue;
                }
            }
        }
    }

    /**
     * Retrieves the amount of xp (scaled by chunk_xp_scale) that recently has
     * been picked up by players in the given our its neighbouring chunks.
     * 
     * @param chunkPos Position of the chunk to check.
     * @return Scaled amount of xp picked up by players.
     */
    public int getXpPool(ChunkPos chunkPos) {
        var xpPool = chunkXpPools.get(chunkPos);
        return xpPool != null ? xpPool.value : 0;
    }

    /**
     * Sets the xp pool of the given chunk to the specified value.
     * 
     * @param chunkPos Position of the chunk.
     * @param value Xp pool value to set.
     */
    public void setXpPool(ChunkPos chunkPos, int value) {
        var xpPool = chunkXpPools.get(chunkPos);

        if(xpPool == null) {
            chunkXpPools.put(chunkPos, new XpPool(value));
        } else {
            xpPool.value = value;
        }
    }
}
