/*
 * This file is part of Skill Issue.
 * Copyright (c) 2024, HDainester, All rights reserved.
 *
 * Skill Issue is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Skill Issue is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along
 * with Skill Issue. If not, see <http://www.gnu.org/licenses/lgpl>.
 */
package com.gitlab.srcmc.skillissue.forge.api.milestones;

import com.gitlab.srcmc.skillissue.ModCommon;
import com.gitlab.srcmc.skillissue.forge.ModForge;
import com.gitlab.srcmc.skillissue.forge.api.milestones.Milestone.CommandType;
import com.gitlab.srcmc.skillissue.forge.capabilities.IPlayerLevelCapability;
import com.gitlab.srcmc.skillissue.forge.collections.RangeList;
import com.mojang.brigadier.exceptions.CommandSyntaxException;

import net.minecraft.network.chat.Component;
import net.minecraft.world.entity.player.Player;

public final class Milestones {
    private Milestones() {}

    public static void updateAttributes(Player player) {
        if(!player.level.isClientSide) {
            var playerLevel = IPlayerLevelCapability.getMaxPlayerLevel(player);

            for(var attMod : ModForge.COMMON_CONFIG.getAttributeModifications()) {
                attMod.apply(player, playerLevel);
            }

            player.setHealth(player.getHealth()); // triggers update on client
        }
    }

    public static void updateRewards(Player player) {
        updateRewards(player, 0);
    }

    public static void updateRewards(Player player, int startLevel) {
        updateRewards(player, startLevel, CommandType.ANY);
    }

    public static void updateRewards(Player player, int startLevel, char commandTypeFlag) {
        updateMilestones(ModForge.COMMON_CONFIG.getMilestoneRewards(player.getUUID()), player, startLevel, commandTypeFlag);
    }

    public static void updatePenalties(Player player) {
        updatePenalties(player, 0);
    }

    public static void updatePenalties(Player player, int startLevel) {
        updatePenalties(player, startLevel, CommandType.ANY);
    }

    public static void updatePenalties(Player player, int startLevel, char commandTypeFlag) {
        updateMilestones(ModForge.COMMON_CONFIG.getMilestonePenalties(player.getUUID()), player, startLevel, commandTypeFlag);
    }

    private static void updateMilestones(RangeList<Milestone> milestones, Player player, int startLevel, char commandTypeFlag) {
        if(!player.level.isClientSide) {
            var playerLevel = IPlayerLevelCapability.getMaxPlayerLevel(player);

            milestones
                .subList(Milestone.dummy(startLevel), Milestone.dummy(playerLevel + 1))
                .stream().filter(milestone -> (milestone.commandsType & commandTypeFlag) != 0 && (milestone.level == playerLevel || milestone.isVolatile))
                .forEach(milestone -> {
                    milestone.commands.forEach(command -> {
                        try {
                            player.getServer().getCommands().getDispatcher().execute(command);
                        } catch (CommandSyntaxException e) {
                            player.createCommandSourceStack().sendFailure(Component.literal(e.getMessage()));
                            ModCommon.LOG.error(e.getMessage(), e);
                        }
                    });
                });
        }
    }
}
