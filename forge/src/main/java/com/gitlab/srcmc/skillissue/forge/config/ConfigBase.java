/*
 * This file is part of Skill Issue.
 * Copyright (c) 2024, HDainester, All rights reserved.
 *
 * Skill Issue is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Skill Issue is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along
 * with Skill Issue. If not, see <http://www.gnu.org/licenses/lgpl>.
 */
package com.gitlab.srcmc.skillissue.forge.config;

import com.gitlab.srcmc.skillissue.forge.ModForge;

import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.config.ModConfig;
import net.minecraftforge.fml.event.config.ModConfigEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;

public abstract class ConfigBase {
    private ModConfig.Type configType;

    protected abstract void onConfigReload(ModConfigEvent event);
    protected abstract void onPlayerLogin(PlayerEvent.PlayerLoggedInEvent event);
    protected abstract void onPlayerLogout(PlayerEvent.PlayerLoggedOutEvent event);
    protected abstract ForgeConfigSpec getSpec();

    public ConfigBase(ModConfig.Type configType) {
        this.configType = configType;
    }

    public void register(ModForge mod) {
        ModLoadingContext.get().registerConfig(configType, getSpec());
		var modBus = FMLJavaModLoadingContext.get().getModEventBus();
        var forgeBus = MinecraftForge.EVENT_BUS;
        modBus.addListener(this::onConfigReload);
        forgeBus.addListener(this::onPlayerLogin);
        forgeBus.addListener(this::onPlayerLogout);
    }
}
