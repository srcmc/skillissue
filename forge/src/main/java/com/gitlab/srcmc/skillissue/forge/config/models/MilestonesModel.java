/*
 * This file is part of Skill Issue.
 * Copyright (c) 2024, HDainester, All rights reserved.
 *
 * Skill Issue is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Skill Issue is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along
 * with Skill Issue. If not, see <http://www.gnu.org/licenses/lgpl>.
 */
package com.gitlab.srcmc.skillissue.forge.config.models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.gitlab.srcmc.skillissue.forge.api.milestones.Milestone;
import com.gitlab.srcmc.skillissue.forge.collections.RangeList;

import net.minecraft.world.entity.player.Player;

public abstract class MilestonesModel implements IModelWithArgs<Map<UUID, RangeList<Milestone>>, Player> {
    protected List<MilestoneModel> milestones = new ArrayList<>();

    @Override
    public void mapToEntity(Map<UUID, RangeList<Milestone>> entity, Player args) {
        var dispatcher = args.getServer().getCommands().getDispatcher();
        var list = entity.get(args.getUUID());

        if(list == null) {
            list = new RangeList<>();
            entity.put(args.getUUID(), list);
        } else {
            list.clear();
        }

        for(var milestone : milestones) {
            milestone.mapToEntity(list, new MilestoneModel.Args(args, dispatcher));
        }

        Collections.sort(list);
    }
}
