/*
 * This file is part of Skill Issue.
 * Copyright (c) 2024, HDainester, All rights reserved.
 *
 * Skill Issue is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Skill Issue is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along
 * with Skill Issue. If not, see <http://www.gnu.org/licenses/lgpl>.
 */
package com.gitlab.srcmc.skillissue.forge.capabilities;

import net.minecraftforge.common.capabilities.AutoRegisterCapability;

@AutoRegisterCapability
public class PlayerLevelCapability implements IPlayerLevelCapability {
    private int maxLevel;

    public PlayerLevelCapability(int playerLevel) {
        maxLevel = playerLevel;
    }

    @Override
    public int getMaxLevel() {
        return maxLevel;
    }

    @Override
    public void setMaxLevel(int level) {
        maxLevel = level;
    }

    @Override
    public boolean updateMaxLevel(int level) {
        if(level > maxLevel) {
            maxLevel = level;
            return true;
        }

        return false;
    }
}
