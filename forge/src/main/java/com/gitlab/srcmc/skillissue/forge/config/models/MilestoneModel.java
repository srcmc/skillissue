/*
 * This file is part of Skill Issue.
 * Copyright (c) 2024, HDainester, All rights reserved.
 *
 * Skill Issue is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Skill Issue is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along
 * with Skill Issue. If not, see <http://www.gnu.org/licenses/lgpl>.
 */
package com.gitlab.srcmc.skillissue.forge.config.models;

import java.util.ArrayList;

import com.gitlab.srcmc.skillissue.ModCommon;
import com.gitlab.srcmc.skillissue.forge.api.milestones.Milestone;
import com.gitlab.srcmc.skillissue.forge.api.milestones.Milestone.CommandType;
import com.gitlab.srcmc.skillissue.forge.collections.RangeList;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.ParseResults;

import net.minecraft.commands.CommandSourceStack;
import net.minecraft.world.entity.player.Player;

public class MilestoneModel implements IModelWithArgs<RangeList<Milestone>, MilestoneModel.Args> {
    public static class Args {
        public final Player player;
        public final CommandDispatcher<CommandSourceStack> dispatcher;

        public Args(Player player, CommandDispatcher<CommandSourceStack> dispatcher) {
            this.player = player;
            this.dispatcher = dispatcher;
        }
    }

    public final int level;
    public final int level_step;
    public final int level_range;
    public final boolean on_level_up;
    public final boolean on_respawn;
    public final boolean on_login;
    public final boolean is_volatile;
    public final String level_inject;
    public final String[] commands;

    public MilestoneModel() {
        this(0, 1, 1, true, false, false, false, "");
    }

    public MilestoneModel(
        int level, int level_step, int level_range,
        boolean on_level_up, boolean on_respawn, boolean on_login, boolean is_volatile,
        String level_inject, String... commands)
    {
        this.level = level;
        this.level_step = level_step;
        this.level_range = level_range;
        this.on_level_up = on_level_up;
        this.on_respawn = on_respawn;
        this.on_login = on_login;
        this.is_volatile = is_volatile;
        this.level_inject = level_inject;
        this.commands = commands;
    }

    @Override
    public void mapToEntity(RangeList<Milestone> entity, Args args) {
        var level_step = this.level_step > 0 ? this.level_step : this.level_range;
        var level_cap = this.level + this.level_range;
        var command_type = (char)((this.on_level_up ? CommandType.LEVEL_UP : 0)
            | (this.on_respawn ? CommandType.RESPAWN : 0)
            | (this.on_login ? CommandType.LOGIN : 0));
        
        for(var level = this.level; level < level_cap; level += level_step) {
            var list = new ArrayList<ParseResults<CommandSourceStack>>();
            entity.add(new Milestone(level, list, command_type, this.is_volatile));
            
            for(var command : this.commands) {
                var literal = this.level_inject != null && !this.level_inject.isBlank()
                    ? command.replace(this.level_inject, String.valueOf(level))
                    : command;

                var parseResults = args.dispatcher.parse(literal, args.player
                    .createCommandSourceStack()
                    .withSuppressedOutput()
                    .withPermission(4));

                if(parseResults.getReader().canRead()) {
                    ModCommon.LOG.error("Invalid command: '" + literal + "'");
                } else {
                    list.add(parseResults);
                }
            }
        }
    }
}
