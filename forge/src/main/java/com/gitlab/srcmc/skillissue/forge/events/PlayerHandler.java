/*
 * This file is part of Skill Issue.
 * Copyright (c) 2024, HDainester, All rights reserved.
 *
 * Skill Issue is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Skill Issue is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along
 * with Skill Issue. If not, see <http://www.gnu.org/licenses/lgpl>.
 */
package com.gitlab.srcmc.skillissue.forge.events;

import com.gitlab.srcmc.skillissue.ModCommon;
import com.gitlab.srcmc.skillissue.forge.ModForge;
import com.gitlab.srcmc.skillissue.forge.api.milestones.Milestones;
import com.gitlab.srcmc.skillissue.forge.api.milestones.Milestone.CommandType;
import com.gitlab.srcmc.skillissue.forge.capabilities.IPlayerLevelCapability;

import net.minecraft.world.entity.player.Player;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.event.entity.player.PlayerXpEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;

@Mod.EventBusSubscriber(modid = ModCommon.MOD_ID, bus = Bus.FORGE)
public class PlayerHandler {
    @SubscribeEvent
    public static void onPlayerXpLevelChange(PlayerXpEvent.LevelChange event) {
        var player = event.getEntity();

        if(player.isAlive()) {
            var playerLevel = player.experienceLevel + 1;
            
            if(IPlayerLevelCapability.updateMaxPlayerLevel(player, playerLevel)) {
                updateRewards(player, playerLevel - event.getLevels() + 1, CommandType.LEVEL_UP);
                updateAttributes(player);
            }
        }
    }

    @SubscribeEvent
    public static void onPlayerXpPickup(PlayerXpEvent.PickupXp event) {
        if(!event.getEntity().level.isClientSide) {
            var chunkPos = event.getEntity().chunkPosition();
            var xpPool = ModForge.CHUNK_XP_POOLS.getXpPool(chunkPos);
            var xpValue = (int)(event.getOrb().value * (1 - (1 - ModForge.COMMON_CONFIG.getChunkXpPenaltyFactor())*xpPool/(double)ModForge.COMMON_CONFIG.getChunkXpThreshold()));
            ModForge.CHUNK_XP_POOLS.addXpToPools(chunkPos, xpValue);
            event.getOrb().value = xpValue;
        }
    }

    @SubscribeEvent
    public static void onPlayerClone(PlayerEvent.Clone event) {
        if(event.isWasDeath()) {
            event.getOriginal().reviveCaps();
            var newLevel = (int)(IPlayerLevelCapability.getMaxPlayerLevel(event.getOriginal())*ModForge.COMMON_CONFIG.getDeathPenaltyLevelFactor());
            event.getOriginal().invalidateCaps();
            IPlayerLevelCapability.setMaxPlayerLevel(event.getEntity(), newLevel);
            updateRewards(event.getEntity(), 0, CommandType.RESPAWN);
            updatePenalties(event.getEntity());
            updateAttributes(event.getEntity());
        }
    }

    @SubscribeEvent
    public static void onPlayerLogin(PlayerEvent.PlayerLoggedInEvent event) {
        updateRewards(event.getEntity(), 0, CommandType.LOGIN);
        updateAttributes(event.getEntity());
    }

    @SubscribeEvent
    public static void onPlayerLogout(PlayerEvent.PlayerLoggedOutEvent event) {
        ModForge.PLAYER_MODIFIERS.dispose(event.getEntity());
    }

    private static void updateAttributes(Player player) {
        var playerLevel = IPlayerLevelCapability.getMaxPlayerLevel(player);

        for(var attMod : ModForge.COMMON_CONFIG.getAttributeModifications()) {
            attMod.apply(player, playerLevel);
        }
    }

    @SuppressWarnings("unused")
    private static void updateRewards(Player player) {
        updateRewards(player, 0);
    }

    private static void updateRewards(Player player, int startLevel) {
        updateRewards(player, startLevel, CommandType.ANY);
    }

    private static void updateRewards(Player player, int startLevel, char commandTypeFlag) {
        Milestones.updateRewards(player, startLevel, commandTypeFlag);
    }

    private static void updatePenalties(Player player) {
        updatePenalties(player, 0);
    }

    private static void updatePenalties(Player player, int startLevel) {
        updatePenalties(player, startLevel, CommandType.ANY);
    }

    private static void updatePenalties(Player player, int startLevel, char commandTypeFlag) {
        Milestones.updatePenalties(player, startLevel, commandTypeFlag);
    }
}
