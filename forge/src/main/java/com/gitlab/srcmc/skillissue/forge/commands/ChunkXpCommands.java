/*
 * This file is part of Skill Issue.
 * Copyright (c) 2024, HDainester, All rights reserved.
 *
 * Skill Issue is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Skill Issue is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along
 * with Skill Issue. If not, see <http://www.gnu.org/licenses/lgpl>.
 */
package com.gitlab.srcmc.skillissue.forge.commands;

import com.gitlab.srcmc.skillissue.ModCommon;
import com.gitlab.srcmc.skillissue.forge.ModForge;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.IntegerArgumentType;

import net.minecraft.commands.CommandSourceStack;
import net.minecraft.commands.Commands;
import net.minecraft.commands.arguments.coordinates.BlockPosArgument;
import net.minecraft.network.chat.Component;

public final class ChunkXpCommands {
    private ChunkXpCommands() {}

    public static void register(CommandDispatcher<CommandSourceStack> dispatcher) {
        dispatcher.register(Commands.literal(ModCommon.MOD_ID)
            .requires(css -> css.hasPermission(1))
            .then(Commands.literal("chunkxp")
                .then(Commands.literal("get")
                    .then(Commands.argument("pos", BlockPosArgument.blockPos())
                        .executes(context -> {
                            var chunkPos = context.getSource().getLevel().getChunk(BlockPosArgument.getSpawnablePos(context, "pos")).getPos();
                            var xpPool = ModForge.CHUNK_XP_POOLS.getXpPool(chunkPos);
                            context.getSource().sendSuccess(Component.literal(String.valueOf(xpPool)), false);
                            return xpPool;
                        })))
                .then(Commands.literal("set")
                    .requires(css -> css.hasPermission(2))
                    .then(Commands.argument("pos", BlockPosArgument.blockPos())
                        .then(Commands.argument("value", IntegerArgumentType.integer(0))
                            .executes(context -> {
                                var chunkPos = context.getSource().getLevel().getChunk(BlockPosArgument.getSpawnablePos(context, "pos")).getPos();
                                var xpPool = IntegerArgumentType.getInteger(context, "value");
                                ModForge.CHUNK_XP_POOLS.setXpPool(chunkPos ,xpPool);
                                return xpPool;
                            }))))));
    }
}
