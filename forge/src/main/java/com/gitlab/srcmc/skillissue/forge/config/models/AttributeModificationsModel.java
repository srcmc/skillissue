/*
 * This file is part of Skill Issue.
 * Copyright (c) 2024, HDainester, All rights reserved.
 *
 * Skill Issue is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Skill Issue is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along
 * with Skill Issue. If not, see <http://www.gnu.org/licenses/lgpl>.
 */
package com.gitlab.srcmc.skillissue.forge.config.models;

import java.util.List;

import com.gitlab.srcmc.skillissue.ModCommon;
import com.gitlab.srcmc.skillissue.forge.api.attributes.AttributeModification;

import net.minecraft.resources.ResourceLocation;
import net.minecraftforge.registries.ForgeRegistries;

public class AttributeModificationsModel implements IModel<List<AttributeModification>> {
    protected static class Attribute {
        public final String key;
        public final double bonus;
        public final double min_factor;
        public final double max_factor;

        public Attribute(String key, double bonus, double min_factor, double max_factor) {
            this.key = key;
            this.bonus = bonus;
            this.min_factor = min_factor;
            this.max_factor = max_factor;
        }
    }

    protected List<Attribute> attributes;

    public AttributeModificationsModel() {
        attributes = List.of(
            new Attribute("generic.max_health", 0, 0.8, 2),
            new Attribute("generic.armor_toughness", 3, 0, 1),
            new Attribute("generic.knockback_resistance", 0.25, 0, 1),
            new Attribute("generic.movement_speed", 0, 0.9, 1.15),
            new Attribute("generic.attack_damage", 0, 0.9, 1.25),
            new Attribute("generic.attack_speed", 0, 0.9, 1.25),
            new Attribute("generic.attack_knockback", 0.25, 0, 1));
    }

    @Override
    public void mapToEntity(List<AttributeModification> entity) {
        entity.clear();

        for(var model : attributes) {
            var attribute = ForgeRegistries.ATTRIBUTES.getValue(getLocation(model.key));

            if(attribute != null) {
                entity.add(new AttributeModification(attribute, model.bonus, model.min_factor, model.max_factor));
            } else {
                ModCommon.LOG.error("Invalid attribute key: " + model.key);
            }
        }
    }

    private static ResourceLocation getLocation(String attributeKey) {
        var location = attributeKey.split(":", 2);
        String modid, path;

        if(location.length == 2) {
            modid = location[0];
            path = location[1];
        } else {
            modid = "minecraft";
            path = location[0];
        }

        return new ResourceLocation(modid, path);
    }
}
