/*
 * This file is part of Skill Issue.
 * Copyright (c) 2024, HDainester, All rights reserved.
 *
 * Skill Issue is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Skill Issue is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along
 * with Skill Issue. If not, see <http://www.gnu.org/licenses/lgpl>.
 */
package com.gitlab.srcmc.skillissue.forge.capabilities;

import com.gitlab.srcmc.skillissue.forge.ModCapabilities;

import net.minecraft.world.entity.player.Player;
import net.minecraftforge.common.capabilities.AutoRegisterCapability;

@AutoRegisterCapability
public interface IPlayerLevelCapability {
    int getMaxLevel();
    void setMaxLevel(int level);
    boolean updateMaxLevel(int level);

    public static int getMaxPlayerLevel(Player player) {
        var cap = player.getCapability(ModCapabilities.LEVEL).orElseGet(null);
        return cap != null ? cap.getMaxLevel() : 0;
    }

    public static void setMaxPlayerLevel(Player player, int level) {
        player.getCapability(ModCapabilities.LEVEL).ifPresent(cap -> cap.setMaxLevel(level));
    }

    public static boolean updateMaxPlayerLevel(Player player, int level) {
        var cap = player.getCapability(ModCapabilities.LEVEL).orElseGet(null);

        if(cap != null && level > cap.getMaxLevel()) {
            cap.setMaxLevel(level);
            return true;
        }

        return false;
    }
}
