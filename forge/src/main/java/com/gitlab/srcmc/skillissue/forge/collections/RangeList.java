/*
 * This file is part of Skill Issue.
 * Copyright (c) 2024, HDainester, All rights reserved.
 *
 * Skill Issue is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Skill Issue is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along
 * with Skill Issue. If not, see <http://www.gnu.org/licenses/lgpl>.
 */
package com.gitlab.srcmc.skillissue.forge.collections;

import java.util.ArrayList;
import java.util.List;

/**
 * An ArrayList that supports access to a range of elements based of their weight.
 * Elements must implement the Comparable interface.
 */
public class RangeList<E extends Comparable<? super E>> extends ArrayList<E> {
    /**
     * Returns a view of the portion of this sorted list whose elements range from
     * fromElement, inclusive, to toElement, exclusive (if fromElement and toElement
     * are equal, the returned list is empty). The sub-list is retrieved in O(logn)
     * time complexity.
     * 
     * @param fromElement low endpoint (inclusive) of the returned list
     * @param toElement high endpoint (exclusive) of the returned list
     * @return a view of the portion of this list whose elements range from
     * fromElement, inclusive, to toElement, exclusive
     */
    public List<E> subList(E fromElement, E toElement) {
        return subList(0, size() - 1, size()/2, fromElement, toElement);
    }

    private List<E> subList(int left, int right, int step, E fromElement, E toElement) {        
        var size = size();
        
        if(step == 0) {
            if(left < 0) {
                left = 0;
            } else {
                while(left > 0 && fromElement.compareTo(get(left)) <= 0) left--;
                while(left < size && fromElement.compareTo(get(left)) > 0) left++;
            }
            
            if(right >= size) {
                right = size - 1;
            } else {
                while(right < size - 1 && toElement.compareTo(get(right)) >= 0) right++;
            }
            
            while(right >= 0 && toElement.compareTo(get(right)) <= 0) right--;
            
            return super.subList(left, right + 1);
        }
        
        if(left >= 0) {
            if(fromElement.compareTo(get(left)) > 0) {
                left += step;
            } else {
                left -= step;
            }
        }
        
        if(right < size) {
            if(toElement.compareTo(get(right)) < 0) {
                right -= step;
            } else {
                right += step;
            }
        }

        return left < 0 && right >= size
            ? super.subList(0, size)
            : subList(left, right, step/2, fromElement, toElement);
    }
}
