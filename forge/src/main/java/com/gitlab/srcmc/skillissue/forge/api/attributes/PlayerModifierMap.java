/*
 * This file is part of Skill Issue.
 * Copyright (c) 2024, HDainester, All rights reserved.
 *
 * Skill Issue is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Skill Issue is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along
 * with Skill Issue. If not, see <http://www.gnu.org/licenses/lgpl>.
 */
package com.gitlab.srcmc.skillissue.forge.api.attributes;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.gitlab.srcmc.skillissue.ModCommon;

import net.minecraft.world.entity.ai.attributes.Attribute;
import net.minecraft.world.entity.ai.attributes.AttributeInstance;
import net.minecraft.world.entity.ai.attributes.AttributeModifier;
import net.minecraft.world.entity.ai.attributes.AttributeModifier.Operation;
import net.minecraft.world.entity.player.Player;

public class PlayerModifierMap {
    private Map<UUID, Map<AttributeInstance, Map<Operation, AttributeModifier>>> playerModifiers = new HashMap<>();

    public ModifierAccess get(Player player, Attribute attribute) {
        return new ModifierAccess(player, attribute);
    }

    public void dispose(Player player) {
        playerModifiers.remove(player.getUUID());
    }

    public class ModifierAccess {
        private Player player;
        private AttributeInstance attribute;

        protected ModifierAccess(@NonNull Player player, @NonNull Attribute attribute) {
            this.player = player;
            this.attribute = player.getAttribute(attribute);
        }

        public void setFactor(double value) {
            apply(value - 1, Operation.MULTIPLY_BASE);
        }

        public void setBonus(double value) {
            apply(value, Operation.ADDITION);
        }

        public void apply(double value, Operation op) {
            var modifiers = playerModifiers.get(player.getUUID());

            if(modifiers == null) {
                modifiers = new HashMap<>();
                playerModifiers.put(player.getUUID(), modifiers);
            }

            var operations = modifiers.get(attribute);

            if(operations == null) {
                operations = new HashMap<>();
                modifiers.put(attribute, operations);
            }

            var modifier = operations.get(op);

            if(modifier != null) {
                attribute.removeModifier(modifier);
            }

            modifier = new AttributeModifier(ModCommon.MOD_ID, value, op);
            operations.put(op, modifier);
            attribute.addTransientModifier(modifier);
        }
    }
}
