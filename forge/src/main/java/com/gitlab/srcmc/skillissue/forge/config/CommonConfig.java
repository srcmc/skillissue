/*
 * This file is part of Skill Issue.
 * Copyright (c) 2024, HDainester, All rights reserved.
 *
 * Skill Issue is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Skill Issue is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along
 * with Skill Issue. If not, see <http://www.gnu.org/licenses/lgpl>.
 */
package com.gitlab.srcmc.skillissue.forge.config;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import com.gitlab.srcmc.skillissue.ModCommon;
import com.gitlab.srcmc.skillissue.forge.api.attributes.AttributeModification;
import com.gitlab.srcmc.skillissue.forge.api.milestones.Milestone;
import com.gitlab.srcmc.skillissue.forge.collections.RangeList;
import com.gitlab.srcmc.skillissue.forge.config.models.AttributeModificationsModel;
import com.gitlab.srcmc.skillissue.forge.config.models.IModelWithArgs;
import com.gitlab.srcmc.skillissue.forge.config.models.MilestonePenaltiesModel;
import com.gitlab.srcmc.skillissue.forge.config.models.MilestoneRewardsModel;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;
import com.google.gson.JsonParseException;

import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.common.ForgeConfigSpec.ConfigValue;
import net.minecraftforge.event.entity.player.PlayerEvent.PlayerLoggedInEvent;
import net.minecraftforge.event.entity.player.PlayerEvent.PlayerLoggedOutEvent;
import net.minecraftforge.fml.config.ModConfig;
import net.minecraftforge.fml.event.config.ModConfigEvent;
import net.minecraftforge.fml.loading.FMLPaths;

public class CommonConfig extends ConfigBase {
    private final ConfigValue<Integer> maxPlayerLevelValue;
    private final ConfigValue<Double> deathPenaltyLevelFactorValue;
    private final ConfigValue<Integer> chunkXpThresholdValue;
    private final ConfigValue<Integer> chunkXpRegerPerTickValue;
    private final ConfigValue<Double> chunkXpScale;
    private final ConfigValue<Double> chunkXpPenaltyFactorValue;
    private final ForgeConfigSpec spec;

    private final Map<UUID, RangeList<Milestone>> milestoneRewards = new HashMap<>();
    private final Map<UUID, RangeList<Milestone>> milestonePenalties = new HashMap<>();
    private final List<AttributeModification> attributeModifications = new ArrayList<>();

    private MilestoneRewardsModel milestoneRewardsModel;
    private MilestonePenaltiesModel milestonePenaltiesModel;
    private AttributeModificationsModel attributeModificationsModel;

    public CommonConfig() {
        this(new ForgeConfigSpec.Builder());
    }

    public CommonConfig(ForgeConfigSpec.Builder builder) {
        super(ModConfig.Type.COMMON);
        builder.push("Player Levels");

        maxPlayerLevelValue = builder
            .comment("The maximum level for a player to gain benefits from 'attribute_modifications'.")
            .defineInRange("max_player_level", 140, 1, Integer.MAX_VALUE);

        deathPenaltyLevelFactorValue = builder
            .comment("This factor is applied to a players level upon death. All 'attribute_modifications' up to the new level remain. Additionally 'milestone_penalties' are applied for the lost levels.")
            .defineInRange("death_penalty_factor", 0.8, 0, 1);

        builder.pop();
        builder.push("Chunk Xp");

        chunkXpThresholdValue = builder
            .comment("This value determines the max 'scaled exp' that can be picked up in an area (3x3 chunks) before the effect of the 'chunk_xp_penalty_factor' is fully applied. This value also affects the time it takes before a chunk has fully recorvered from the penalty.")
            .defineInRange("chunk_xp_threshold", 48000, 1, Integer.MAX_VALUE);

        chunkXpRegerPerTickValue = builder
            .comment("This value determines the speed per tick at which a chunk regenerates from its accumulated xp.")
            .defineInRange("chunk_xp_regen_per_tick", 1, 1, Integer.MAX_VALUE);

        chunkXpScale = builder
            .comment("This factor is applied to any xp picked up by players before beeing added to a chunks xp pool. This value affects the time it takes before a chunk has reached its xp threshold. Setting this to 0 will completely disable all chunk xp features.")
            .defineInRange("chunk_xp_scale", 20, 0, Double.MAX_VALUE);

        chunkXpPenaltyFactorValue = builder
            .comment("This value determines the minimum factor that can be applied to any xp picked up by a player. The factor linearly shrinks from 1 towards this value based of the xp pool in the players chunk in relation to the 'chunk_xp_threshold'.")
            .defineInRange("chunk_xp_penalty_factor", 0.2, 0, 1);

        spec = builder.build();
    }

    // player levels
    public int getMaxPlayerLevel() {
        return this.maxPlayerLevelValue.get();
    }

    public double getDeathPenaltyLevelFactor() {
        return this.deathPenaltyLevelFactorValue.get();
    }

    // player milestones
    public RangeList<Milestone> getMilestoneRewards(UUID uuid) {
        var list = milestoneRewards.get(uuid);
        return list != null ? list : new RangeList<>();
    }
    
    public RangeList<Milestone> getMilestonePenalties(UUID uuid) {
        var list = milestonePenalties.get(uuid);
        return list != null ? list : new RangeList<>();
    }
    
    // player modifiers
    public Iterable<AttributeModification> getAttributeModifications() {
        return attributeModifications;
    }

    // chunk xp
    public int getChunkXpThreshold() {
        return this.chunkXpThresholdValue.get();
    }
    
    public int getChunkXpRegenPerTick() {
        return this.chunkXpRegerPerTickValue.get();
    }

    public double getChunkXpScale() {
        return this.chunkXpScale.get();
    }

    public double getChunkXpPenaltyFactor() {
        return this.chunkXpPenaltyFactorValue.get();
    }

    @Override
    protected ForgeConfigSpec getSpec() {
        return spec;
    }

    @Override
    protected void onConfigReload(ModConfigEvent event) {
        loadJsonConfigs();
    }

    @Override
    protected void onPlayerLogin(PlayerLoggedInEvent event) {
        if(milestoneRewardsModel != null) milestoneRewardsModel.mapToEntity(milestoneRewards, event.getEntity());
        if(milestonePenaltiesModel != null) milestonePenaltiesModel.mapToEntity(milestonePenalties, event.getEntity());
        if(attributeModificationsModel != null) attributeModificationsModel.mapToEntity(attributeModifications);
    }

    @Override
    protected void onPlayerLogout(PlayerLoggedOutEvent event) {
        milestoneRewards.remove(event.getEntity().getUUID());
        milestonePenalties.remove(event.getEntity().getUUID());
    }

    private static <Model extends IModelWithArgs<T, Args>, T, Args> Model loadModel(String modelFilePath, Class<Model> clazz) {
        var configDirPath = FMLPaths.CONFIGDIR.get().toString() + File.separatorChar + ModCommon.MOD_ID;
        Model model = null;

        var modelFile = new File(configDirPath + File.separatorChar + modelFilePath);
        var gson = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create();

        try(var fr = new FileReader(modelFile)) {
            model = gson.fromJson(fr, clazz);
        } catch(JsonParseException | IOException e1) {
            ModCommon.LOG.warn("Failed to load config from '" + modelFile + "': Running with default config");
            
            try {
                model = clazz.getConstructor().newInstance();

                if(e1 instanceof FileNotFoundException) {
                    try(var fw = new FileWriter(modelFile)) {
                        gson.toJson(model, clazz, gson.newJsonWriter(fw));
                    } catch(JsonIOException | IOException e2) {
                        ModCommon.LOG.warn("Failed to create default config at '" + modelFile + "'");
                    }
                }
            } catch(InstantiationException | IllegalAccessException | IllegalArgumentException
                | InvocationTargetException | NoSuchMethodException | SecurityException e3)
            {
                ModCommon.LOG.error("Could not instantiate '" + clazz.getName() + "': Please contact mod author", e3);
            }
        }

        return model;
    }

    private void loadJsonConfigs() {
        new File(FMLPaths.CONFIGDIR.get().toString() + File.separatorChar + ModCommon.MOD_ID).mkdirs();
        milestoneRewardsModel = loadModel("rewards.json", MilestoneRewardsModel.class);
        milestonePenaltiesModel = loadModel("penalties.json", MilestonePenaltiesModel.class);
        attributeModificationsModel = loadModel("attributes.json", AttributeModificationsModel.class);
        ModCommon.LOG.info("Loaded json configs");
    }
}
