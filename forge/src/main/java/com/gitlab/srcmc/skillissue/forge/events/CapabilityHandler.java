/*
 * This file is part of Skill Issue.
 * Copyright (c) 2024, HDainester, All rights reserved.
 *
 * Skill Issue is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Skill Issue is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along
 * with Skill Issue. If not, see <http://www.gnu.org/licenses/lgpl>.
 */
package com.gitlab.srcmc.skillissue.forge.events;

import com.gitlab.srcmc.skillissue.ModCommon;
import com.gitlab.srcmc.skillissue.forge.capabilities.provider.PlayerLevelCapabilityProvider;

import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.player.Player;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;

@Mod.EventBusSubscriber(modid = ModCommon.MOD_ID, bus = Bus.FORGE)
public class CapabilityHandler {
    @SubscribeEvent
    public static void onAttachCapabilitiesToEntity(AttachCapabilitiesEvent<Entity> event) {
        if(event.getObject() instanceof Player player) {
            event.addCapability(new ResourceLocation(ModCommon.MOD_ID, "level"), new PlayerLevelCapabilityProvider(player));
        }
    }
}
