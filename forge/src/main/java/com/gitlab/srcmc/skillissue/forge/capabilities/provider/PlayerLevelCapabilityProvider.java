/*
 * This file is part of Skill Issue.
 * Copyright (c) 2024, HDainester, All rights reserved.
 *
 * Skill Issue is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Skill Issue is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along
 * with Skill Issue. If not, see <http://www.gnu.org/licenses/lgpl>.
 */
package com.gitlab.srcmc.skillissue.forge.capabilities.provider;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import com.gitlab.srcmc.skillissue.forge.ModCapabilities;
import com.gitlab.srcmc.skillissue.forge.capabilities.IPlayerLevelCapability;
import com.gitlab.srcmc.skillissue.forge.capabilities.PlayerLevelCapability;

import net.minecraft.core.Direction;
import net.minecraft.nbt.IntTag;
import net.minecraft.nbt.Tag;
import net.minecraft.world.entity.player.Player;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.LazyOptional;

public class PlayerLevelCapabilityProvider implements ICapabilitySerializable<Tag> {
    private final LazyOptional<IPlayerLevelCapability> optional;

    public PlayerLevelCapabilityProvider(Player player) {
        optional = LazyOptional.of(() -> new PlayerLevelCapability(player.experienceLevel + 1));
    }

    @Override
    public <T> @NotNull LazyOptional<T> getCapability(@NotNull Capability<T> cap, @Nullable Direction side) {
        return cap == ModCapabilities.LEVEL ? optional.cast() : LazyOptional.empty();
    }

    @Override
    public Tag serializeNBT() {
        if(optional.isPresent()) {
            return IntTag.valueOf(optional.orElse(null).getMaxLevel());
        }

        return IntTag.valueOf(0);
    }

    @Override
    public void deserializeNBT(Tag nbt) {
        if(optional.isPresent()) {
            if (!(nbt instanceof IntTag intNbt))
                throw new IllegalArgumentException("Can not deserialize to an instance that isn't the default implementation");

            optional.orElse(null).setMaxLevel(intNbt.getAsInt());
        }
    }
}
