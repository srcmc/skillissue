# Changelog

## [1.0.3-beta] - 2024-03-07

***Fixed***

- 'bonus' value not beeing added to attributes

## [1.0.2-beta] - 2024-03-06

***Fixed***

- Modded attributes not working

## [1.0.1-beta] - 2024-03-02

***Fixed***

- Minor health desync on client when using commands to lower milestone level
- Potential incompatibility with other mods (e.g. Xp Tome) causing a crash

## [1.0.0-beta] - 2024-02-28

***Added***

- Block: Skill Pressure Plate
- Block: Xp Sensor
- Feature: Milestone levels
- Feature: Player attributes growth
- Feature: Reward and penalty commands
